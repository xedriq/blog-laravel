function handleDelete(id) {
    let form = document.querySelector('#deleteCategoryForm')
    form.action = `/categories/${id}`
    $('#deleteModal').modal('show')
}