@extends('layouts.app')

@section('content')

<div class="d-flex justify-content-end mb-2">
    <a href="{{ route('categories.create') }}" class="btn btn-primary float-right">Create Category</a>
</div>
<div class="card text-white bg-secondary mb-3">
    <div class="card-header">Categories</div>
    <div class="card-body">

        @if($categories->count() > 0)
        <table class="table table-hover">
            <tbody>
                @foreach ($categories as $category)
                    <tr>
                        <td>{{ $category->name}}</td>
                        <td>
                            <a href="{{ route('categories.edit', $category->id) }}" class="btn btn-info btn-sm">Edit</a>
                            <button class="btn btn-danger btn-sm" onclick="handleDelete({{ $category->id }})">Delete</button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        @else
        <h3>No categories yet.</h3>
        @endif

        <form action="" method="POST" id="deleteCategoryForm">
            @csrf
            @method('DELETE')
            <div class="modal fade text-dark" id="deleteModal">
                <div class="modal-dialog" role="document">
                    <div class="modal-content ">
                    <div class="modal-header bg-warning">
                        <h5 class="modal-title">Delete Category</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure you want to delete this category?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger">Yes, delete.</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal">No, go back.</button>
                    </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection