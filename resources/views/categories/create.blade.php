@extends('layouts.app')

@section('content')
{{-- @if($errors->has('name'))
{{ dd($errors->first()) }}
@endif --}}

{{-- {{ dd(isset($category)) }} --}}
<div class="card text-dark bg-active mb-3">
    <div class="card-header">
        {{ isset($category) ? 'Edit Category' : 'Create Category' }}
        
    </div>
    <div class="card-body">
        <form action="{{ isset($category) ? route('categories.update', $category->id) : route('categories.store') }}" 
            method="POST">
            @csrf
            @if(isset($category))
                @method('PUT')
            @endif
            <div class="form-group">
                <label for="name" class="form0-control-label">Name: </label>
                <input type="text" class="form-control
                {{ $errors->has('name') ? 'is-invalid' : ''}}
                " name="name" id="name" value="{{ isset($category) ? $category->name : '' }}">
                @if($errors->has('name'))
                    <div class="invalid-feedback">{{ $errors->first() }}</div>
                @endif
            </div>
            <button class="btn btn-success" type="submit">
                {{ isset($category) ? 'Update Category' : 'Add Category'}}    
            </button>
        </form>
    </div>
</div>

@endsection